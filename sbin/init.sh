#!/usr/bin/env bash

set -euo pipefail

# defines system defaults (alpine realpath - minus the switches)
EXEC=$(realpath "${0#./}")
EXEC_BASE="${EXEC%/*}"
EXEC_NAME=$(basename "$EXEC")
EXEC_ARGS=$#
EXEC_PARENT="${EXEC_BASE%/*}"

WS_WORKSPACE="${WS_WORKSPACE:-${EXEC_PARENT}}"
WS_SOURCE="${WS_WORKSPACE}/src/workspace"
WS_MODULE="networks-prd"

export TF_CLI_CONFIG_FILE="${WS_SOURCE}/${WS_MODULE}/.terraformrc"

cd ${WS_SOURCE}/${WS_MODULE}

terraform workspace new ${WS_MODULE} || true
terraform workspace select ${WS_MODULE} || true

terraform workspace list

cd ${WS_SOURCE}/${WS_MODULE}

terraform init
terraform plan 

echo -n "Apply plan?  (y/n) "
read answer 

if [[ $answer =~ ^([Yy])$ ]]
	then
		terraform apply
	else 
		echo No
fi

#echo -n "Destroy? (y/n)"
#read destroy

#if [[ $destroy =~ ^([Yy])$ ]]
	#then
		#terraform destroy
	#else 
		#echo No
#fi