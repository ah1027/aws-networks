# main_variables.tfvars 
# file used to set core variables that will be used across multiple modules.

variable "tunnel1_key"  { default = "S9OxdMwfhTq931oFINr4hS5yDdSvH1vF" }
variable "tunnel2_key"  { default = "hK7SWWp6GklSSMmjnkmF5.fkNARoNDc5" }
variable "tunnel1_cidr" { default = "169.254.15.116/30" }
variable "tunnel2_cidr" { default = "169.254.175.244/30" }


variable "all_tags" {
  default = {}
  description = "all tags"
  type = map(string)
}