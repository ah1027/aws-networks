# main.tf 


module "transit-gateway" {
  source = "../../modules//aws/transit-gateway"
}

module "vpc" {
  source = "../../modules/aws/vpc"
  # no depenancies
  # VPC Vars, change here if needed.
  security_vpc_cidr                   = "10.143.0.0/20"
  security_vpc_data_subnet_cidr1      = "10.143.1.0/24"
  security_vpc_data_subnet_cidr2      = "10.143.9.0/24"
  security_vpc_relay_subnet_cidr1     = "10.143.0.0/24"
  security_vpc_relay_subnet_cidr2     = "10.143.8.0/24"
  security_vpc_mgmt_subnet_cidr1      = "10.143.3.0/24"
  security_vpc_mgmt_subnet_cidr2      = "10.143.11.0/24"
  security_vpc_heartbeat_subnet_cidr1 = "10.143.2.0/24"
  security_vpc_heartbeat_subnet_cidr2 = "10.143.10.0/24"
  mgmt_cidr                           = "10.143.16.0/20"
  mgmt_private_subnet_cidr1           = "10.143.16.0/24"
  mgmt_private_subnet_cidr2           = "10.143.17.0/24"
}

module "vpc-att" {
  source = "../../modules/aws/vpc-att"
  # Transit gateway module variables imported.
  tgw_id             = module.transit-gateway.tgw_id
  tgw_vpc_sec_rt_id  = module.transit-gateway.vpc_sec_rt_id
  tgw_vpc_mgmt_rt_id = module.transit-gateway.vpc_mgmt_rt_id
  # vpc module variables imported.
  vpc_sec_id         = module.vpc.vpc_sec_id
  vpc_sec_att_sub1   = module.vpc.vpc_sec_att_sub1
  vpc_sec_att_sub2   = module.vpc.vpc_sec_att_sub2
  vpc_mgmt_id        = module.vpc.vpc_mgmt_id
  vpc_mgmt_att_sub1  = module.vpc.vpc_mgmt_att_sub1
  vpc_mgmt_att_sub2  = module.vpc.vpc_mgmt_att_sub2

  depends_on = [ module.transit-gateway, module.vpc ]
}

module "vpc-route-table" {
  source = "../../modules/aws/vpc-route-table"

  vpc_sec_id              = module.vpc.vpc_sec_id
  mgmt_vpc_route_table_id = module.vpc.vpc_mgmt_default_rt
  vpc_sec_mgmt_sub1       = module.vpc.vpc_sec_mgmt_sub1
  vpc_sec_mgmt_sub2       = module.vpc.vpc_sec_mgmt_sub2
  vpc_sec_data_sub1       = module.vpc.vpc_sec_data_sub1
  vpc_sec_data_sub2       = module.vpc.vpc_sec_data_sub2
  vpc_sec_relay_sub1      = module.vpc.vpc_sec_relay_sub1
  vpc_sec_relay_sub2      = module.vpc.vpc_sec_relay_sub2

  depends_on = [ module.vpc ]
}

module "fortigate" {
  source = "../../modules/aws/fortigate"

  vpc_sec_id              = module.vpc.vpc_sec_id
  vpc_mgmt_id             = module.vpc.vpc_mgmt_id
  vpc_sec_data_sub1       = module.vpc.vpc_sec_data_sub1
  vpc_sec_data_sub2       = module.vpc.vpc_sec_data_sub2
  vpc_sec_heart_sub1      = module.vpc.vpc_sec_heart_sub1
  vpc_sec_heart_sub2      = module.vpc.vpc_sec_heart_sub2
  vpc_sec_mgmt_sub1       = module.vpc.vpc_sec_mgmt_sub1
  vpc_sec_mgmt_sub2       = module.vpc.vpc_sec_mgmt_sub2
  security_vpc_cidr                   = "10.143.0.0/20"
  security_vpc_data_subnet_cidr1      = "10.143.1.0/24"
  security_vpc_data_subnet_cidr2      = "10.143.9.0/24"
  security_vpc_relay_subnet_cidr1     = "10.143.0.0/24"
  security_vpc_relay_subnet_cidr2     = "10.143.8.0/24"
  security_vpc_mgmt_subnet_cidr1      = "10.143.3.0/24"
  security_vpc_mgmt_subnet_cidr2      = "10.143.11.0/24"
  security_vpc_heartbeat_subnet_cidr1 = "10.143.2.0/24"
  security_vpc_heartbeat_subnet_cidr2 = "10.143.10.0/24"
  mgmt_cidr                           = "10.143.16.0/20"
  mgmt_private_subnet_cidr1           = "10.143.16.0/24"
  mgmt_private_subnet_cidr2           = "10.143.17.0/24"

  depends_on = [ module.vpc]
}

module "vpn" {
  source = "../../modules/aws/vpn"

  tgw_id       = module.transit-gateway.tgw_id
  tgw_vpn_rt   = module.transit-gateway.vpn_campus_rt_id
  tunnel1_key  = "${var.tunnel1_key}"
  tunnel2_key  = "${var.tunnel2_key}"
  tunnel1_cidr = "${var.tunnel1_cidr}"
  tunnel2_cidr = "${var.tunnel2_cidr}"
}

module "vpc-routes" {
  source = "../../modules/aws/vpc-routes"

  vpc_mgmt_route_table_id        = module.vpc-route-table.vpc_mgmt_rt
  tgw_terraform_relay_rt         = module.vpc-route-table.vpc_sec_relay_rt
  tgw_terraform_data_and_mgmt_rt = module.vpc-route-table.vpc_sec_main_rt
  tgw_id                         = module.transit-gateway.tgw_id
  vpc_sec_igw_id                 = module.vpc.vpc_sec_igw
  vpc_mgmt_cidr                  = module.vpc.vpc_mgmt_cidr
  fgt_eni                        = module.fortigate.fgt_eni

  depends_on = [ module.vpc-route-table, module.transit-gateway, module.fortigate, module.vpn]
}

module "transit-gateway-routes" {
  source = "../../modules/aws/transit-gateway-routes" 
  
  tgw_vpn_campus_rt    = module.transit-gateway.vpn_campus_rt_id
  tgw_vpc_mgmt_rt      = module.transit-gateway.vpc_mgmt_rt_id
  tgw_vpc_sec_rt       = module.transit-gateway.vpc_sec_rt_id
  tgw_vpc_spokes_rt    = module.transit-gateway.spoke_route_table_ID

  tgw_att_vpc_sec      = module.vpc-att.vpc_sec_att
  tgw_att_vpc_mgmt     = module.vpc-att.vpc_mgmt_att
  tgw_att_vpn_campus   = module.vpn.vpn_att_campus

  depends_on = [ module.vpn, module.fortigate, module.transit-gateway]
}

module "route-53" {
  source = "../../modules/aws/route-53"
}