# main_variables.tfvars 
# file used to set core variables that will be used across multiple modules.

# Variables are set blank. To set the actual values, do that in variables.auto.tfvars.

variable "tunnel1_key"  {  }
variable "tunnel2_key"  {  }
variable "tunnel1_cidr" {  }
variable "tunnel2_cidr" {  }


variable "all_tags" {
  default = {}
  description = "all tags"
  type = map(string)
}

variable "all_vpn_routes" {
  type = list(string)
  default = []
}



variable "security_vpc_cidr" {}
variable "security_vpc_data_subnet_cidr1" {}
variable "security_vpc_data_subnet_cidr2" {}
variable "security_vpc_relay_subnet_cidr1" {}
variable "security_vpc_relay_subnet_cidr2" {}
variable "security_vpc_mgmt_subnet_cidr1" {}
variable "security_vpc_mgmt_subnet_cidr2" {}
variable "security_vpc_heartbeat_subnet_cidr1" {}
variable "security_vpc_heartbeat_subnet_cidr2" {}
variable "mgmt_cidr" {}
variable "mgmt_private_subnet_cidr1" {}
variable "mgmt_private_subnet_cidr2" {}