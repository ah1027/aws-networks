# main.tf 

module "transit-gateway" {
  source = "../../modules//aws/transit-gateway"

  # Variables defined in variables.auto.tfvars
  all_tags             = var.all_tags
}

module "vpc" {
  source = "../../modules/aws/vpc"

  # Variables defined in variables.auto.tfvars
  security_vpc_cidr                   = var.security_vpc_cidr
  security_vpc_data_subnet_cidr1      = var.security_vpc_data_subnet_cidr1
  security_vpc_data_subnet_cidr2      = var.security_vpc_data_subnet_cidr2
  security_vpc_relay_subnet_cidr1     = var.security_vpc_relay_subnet_cidr1
  security_vpc_relay_subnet_cidr2     = var.security_vpc_relay_subnet_cidr2
  security_vpc_mgmt_subnet_cidr1      = var.security_vpc_mgmt_subnet_cidr1
  security_vpc_mgmt_subnet_cidr2      = var.security_vpc_mgmt_subnet_cidr2
  security_vpc_heartbeat_subnet_cidr1 = var.security_vpc_heartbeat_subnet_cidr1
  security_vpc_heartbeat_subnet_cidr2 = var.security_vpc_heartbeat_subnet_cidr2
  mgmt_cidr                           = var.mgmt_cidr
  mgmt_private_subnet_cidr1           = var.mgmt_private_subnet_cidr1
  mgmt_private_subnet_cidr2           = var.mgmt_private_subnet_cidr2

  all_tags                            = var.all_tags
}

module "vpc-att" {
  source = "../../modules/aws/vpc-att"
  
  # Variables imported from modules
  tgw_id             = module.transit-gateway.tgw_id
  tgw_vpc_sec_rt_id  = module.transit-gateway.vpc_sec_rt_id
  tgw_vpc_mgmt_rt_id = module.transit-gateway.vpc_mgmt_rt_id
  vpc_sec_id         = module.vpc.vpc_sec_id
  vpc_sec_att_sub1   = module.vpc.vpc_sec_att_sub1
  vpc_sec_att_sub2   = module.vpc.vpc_sec_att_sub2
  vpc_mgmt_id        = module.vpc.vpc_mgmt_id
  vpc_mgmt_att_sub1  = module.vpc.vpc_mgmt_att_sub1
  vpc_mgmt_att_sub2  = module.vpc.vpc_mgmt_att_sub2

  all_tags           = var.all_tags

  depends_on = [ module.transit-gateway, module.vpc ]
}

module "vpc-route-table" {
  source = "../../modules/aws/vpc-route-table"

  # Variables imported from modules
  vpc_sec_id              = module.vpc.vpc_sec_id
  mgmt_vpc_route_table_id = module.vpc.vpc_mgmt_default_rt
  vpc_sec_mgmt_sub1       = module.vpc.vpc_sec_mgmt_sub1
  vpc_sec_mgmt_sub2       = module.vpc.vpc_sec_mgmt_sub2
  vpc_sec_data_sub1       = module.vpc.vpc_sec_data_sub1
  vpc_sec_data_sub2       = module.vpc.vpc_sec_data_sub2
  vpc_sec_relay_sub1      = module.vpc.vpc_sec_relay_sub1
  vpc_sec_relay_sub2      = module.vpc.vpc_sec_relay_sub2

  all_tags                = var.all_tags

  depends_on = [ module.vpc ]
}

module "fortigate" {
  source = "../../modules/aws/fortigate"

  # Variables imported from modules
  vpc_sec_id              = module.vpc.vpc_sec_id
  vpc_mgmt_id             = module.vpc.vpc_mgmt_id
  vpc_sec_data_sub1       = module.vpc.vpc_sec_data_sub1
  vpc_sec_data_sub2       = module.vpc.vpc_sec_data_sub2
  vpc_sec_heart_sub1      = module.vpc.vpc_sec_heart_sub1
  vpc_sec_heart_sub2      = module.vpc.vpc_sec_heart_sub2
  vpc_sec_mgmt_sub1       = module.vpc.vpc_sec_mgmt_sub1
  vpc_sec_mgmt_sub2       = module.vpc.vpc_sec_mgmt_sub2

  # Variables defined in variables.auto.tfvars
  security_vpc_cidr                   = var.security_vpc_cidr
  security_vpc_data_subnet_cidr1      = var.security_vpc_data_subnet_cidr1
  security_vpc_data_subnet_cidr2      = var.security_vpc_data_subnet_cidr2
  security_vpc_relay_subnet_cidr1     = var.security_vpc_relay_subnet_cidr1
  security_vpc_relay_subnet_cidr2     = var.security_vpc_relay_subnet_cidr2
  security_vpc_mgmt_subnet_cidr1      = var.security_vpc_mgmt_subnet_cidr1
  security_vpc_mgmt_subnet_cidr2      = var.security_vpc_mgmt_subnet_cidr2
  security_vpc_heartbeat_subnet_cidr1 = var.security_vpc_heartbeat_subnet_cidr1
  security_vpc_heartbeat_subnet_cidr2 = var.security_vpc_heartbeat_subnet_cidr2
  mgmt_cidr                           = var.mgmt_cidr
  mgmt_private_subnet_cidr1           = var.mgmt_private_subnet_cidr1
  mgmt_private_subnet_cidr2           = var.mgmt_private_subnet_cidr2

  all_tags                            = var.all_tags

  depends_on = [ module.vpc]
}

module "vpn" {
  source = "../../modules/aws/vpn"

  # Variables imported from modules
  tgw_id       = module.transit-gateway.tgw_id
  tgw_vpn_rt   = module.transit-gateway.vpn_campus_rt_id

  # Variables defined in variables.auto.tfvars
  tunnel1_key  = "${var.tunnel1_key}"
  tunnel2_key  = "${var.tunnel2_key}"
  tunnel1_cidr = "${var.tunnel1_cidr}"
  tunnel2_cidr = "${var.tunnel2_cidr}"
  all_tags     = var.all_tags

  depends_on = [ module.transit-gateway ]
}

module "vpc-routes" {
  source = "../../modules/aws/vpc-routes"

  # Variables imported from modules
  vpc_mgmt_route_table_id        = module.vpc-route-table.vpc_mgmt_rt
  tgw_terraform_relay_rt         = module.vpc-route-table.vpc_sec_relay_rt
  tgw_terraform_data_and_mgmt_rt = module.vpc-route-table.vpc_sec_main_rt
  tgw_id                         = module.transit-gateway.tgw_id
  vpc_sec_igw_id                 = module.vpc.vpc_sec_igw
  vpc_mgmt_cidr                  = module.vpc.vpc_mgmt_cidr
  fgt_eni                        = module.fortigate.fgt_eni

  # Variables defined in variables.auto.tfvars
  vpn_routes                     = var.all_vpn_routes
  all_tags                       = var.all_tags

  depends_on = [ module.vpc-route-table, module.transit-gateway, module.fortigate, module.vpn]
}

module "transit-gateway-routes" {
  source = "../../modules/aws/transit-gateway-routes" 
  
  tgw_vpn_campus_rt    = module.transit-gateway.vpn_campus_rt_id
  tgw_vpc_mgmt_rt      = module.transit-gateway.vpc_mgmt_rt_id
  tgw_vpc_sec_rt       = module.transit-gateway.vpc_sec_rt_id
  tgw_vpc_spokes_rt    = module.transit-gateway.spoke_route_table_ID

  tgw_att_vpc_sec      = module.vpc-att.vpc_sec_att
  tgw_att_vpc_mgmt     = module.vpc-att.vpc_mgmt_att
  tgw_att_vpn_campus   = module.vpn.vpn_att_campus

  # Variables defined in variables.auto.tfvars
  vpn_routes           = var.all_vpn_routes
  all_tags             = var.all_tags

  depends_on = [ module.vpn, module.fortigate, module.transit-gateway]
}

module "route-53" {
  source = "../../modules/aws/route-53"

  # Variables defined in variables.auto.tfvars
  all_tags             = var.all_tags

  depends_on = [ module.fortigate ]
}




