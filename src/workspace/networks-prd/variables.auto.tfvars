tunnel1_key = "#"
tunnel2_key = "#"
tunnel1_cidr = "169.254.15.116/30"
tunnel2_cidr = "169.254.175.244/30"


all_tags = {
  CostCode  = "16144-G-00-1037-2984-@@@@@"
  Owner     = "Networks"
  Email     = "exeit-networks@exeter.ac.uk"
}

all_vpn_routes = [
  "10.26.0.0/24",     # regus
  "144.173.2.0/24",   # dc prd
  "144.173.3.0/24",   # dc prd
  "144.173.5.0/24",   # dc prd
  "144.173.24.0/24",  # st lukes IT
  "144.173.113.0/24", # dc prd
  "144.173.123.0/24", # network test
  "144.173.137.0/24", # northcote house vc2
  "144.173.196.0/24", # dc prd
  "144.173.198.0/24", # dc prd
  "144.173.199.0/24", # dc prd
]


# security vpc full cidr e.g. 10.0.0.0/20
security_vpc_cidr                   = "10.143.0.0/20"

# second subnet of both AZs
security_vpc_data_subnet_cidr1      = "10.143.1.0/24"
security_vpc_data_subnet_cidr2      = "10.143.9.0/24"

# First subnet of both AZs
security_vpc_relay_subnet_cidr1     = "10.143.0.0/24"
security_vpc_relay_subnet_cidr2     = "10.143.8.0/24"

# management subnets for vpc sec
security_vpc_mgmt_subnet_cidr1      = "10.143.3.0/24"
security_vpc_mgmt_subnet_cidr2      = "10.143.11.0/24"

# hearbeat subnets for the firewalls in vpc sec
security_vpc_heartbeat_subnet_cidr1 = "10.143.2.0/24"
security_vpc_heartbeat_subnet_cidr2 = "10.143.10.0/24"

# mgmt vpc details.
mgmt_cidr                           = "10.143.16.0/20"
mgmt_private_subnet_cidr1           = "10.143.16.0/24"
mgmt_private_subnet_cidr2           = "10.143.17.0/24"