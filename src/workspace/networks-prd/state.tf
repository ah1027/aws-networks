# state.tf

# used for backend connection

terraform {
  backend "remote" {
    hostname     = "app.terraform.io"
    organization = "<YOUR-ORG-ID-GOES-HERE>"
    workspaces {
      prefix = "<YOUR-WORKSPACE-PREFIX-GOES-HERE>-"
    }
  }
}
