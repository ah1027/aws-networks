# vpc-routes variables.tf

variable "vpc_mgmt_route_table_id" { default = "" }
variable "tgw_terraform_relay_rt" { default = "" }
variable "tgw_terraform_data_and_mgmt_rt" { default = "" }
variable "tgw_id" { default = "" }
variable "igw_id" { default = "" }
variable "vpc_sec_igw_id" { default = "" }
variable "vpc_mgmt_cidr" { default = "" }
variable "fgt_eni" { default = "" }

variable "vpn_routes" {
  type = list(string)
}

variable "all_tags" {
  default = {}
  description = "all tags"
  type = map(string)
}

