# vpc-routes main.tf

#####################
# MGMT Routes
#####################
/*
List of routes for route table mgmt-rt:

0.0.0.0/0   -> TGW

*/
resource "aws_route" "vpc_mgmt_default_route" {
  route_table_id          = var.vpc_mgmt_route_table_id
  destination_cidr_block  = "0.0.0.0/0"
  transit_gateway_id      = var.tgw_id
}

#####################
# SEC RELAY Routes
#####################
/*
List of routes for route table TGW-terraform-relay-rt:

0.0.0.0/0   -> ENI Interface of firewall

*/
## route table TGW-terraform-relay-rt
# route for vpc_sec relay
resource "aws_route" "vpc_sec_relay_route" {
  route_table_id          = var.tgw_terraform_relay_rt
  destination_cidr_block  = "0.0.0.0/0"
  network_interface_id      = var.fgt_eni
}


#####################
# SEC Routes
#####################
/*
List of routes for route table TGW-terraform-data-and-mgmt-rt:

Specific CIDR   -> TGW
0.0.0.0/0       -> IGW

These routes are populated by the integration team when they build their VPCs:

10.139.0.0/20   -> TGW
10.140.0.0/20   -> TGW
10.141.0.0/20   -> TGW
10.142.0.0/20   -> TGW

*/

# default route to IGW
resource "aws_route" "vpc_sec_default_route" {
  route_table_id          = var.tgw_terraform_data_and_mgmt_rt
  destination_cidr_block  = "0.0.0.0/0"
  gateway_id      = var.vpc_sec_igw_id
}

# route to mgmt via tgw
resource "aws_route" "vpc_sec_route1" {
  route_table_id          = var.tgw_terraform_data_and_mgmt_rt
  destination_cidr_block  = var.vpc_mgmt_cidr
  transit_gateway_id      = var.tgw_id
}

