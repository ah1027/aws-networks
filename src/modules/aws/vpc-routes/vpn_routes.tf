# vpn_routes.tf

#####################
# SEC Routes
#####################
/*
List of VPN routes for route table TGW-terraform-data-and-mgmt-rt:

Specific CIDR   -> TGW

*/

# VPN CAMPUS - DC-PRD
resource "aws_route" "vpn_route1" {
  route_table_id          = var.tgw_terraform_data_and_mgmt_rt
  destination_cidr_block  = "144.173.6.0/24"
  transit_gateway_id      = var.tgw_id
}
# VPN CAMPUS - VIA VPN Range 1
resource "aws_route" "vpn_route2" {
  route_table_id          = var.tgw_terraform_data_and_mgmt_rt
  destination_cidr_block  = "10.46.128.0/23"
  transit_gateway_id      = var.tgw_id
}
# VPN CAMPUS - VIA VPN Range 2
resource "aws_route" "vpn_route3" {
  route_table_id          = var.tgw_terraform_data_and_mgmt_rt
  destination_cidr_block  = "10.46.130.0/24"
  transit_gateway_id      = var.tgw_id
}
# VPN CAMPUS - See in variables, runs through the list and adds them all.
resource "aws_route" "vpn_route4" {
  count                   = length(var.vpn_routes)

  route_table_id          = var.tgw_terraform_data_and_mgmt_rt
  destination_cidr_block  = var.vpn_routes[count.index]
  transit_gateway_id      = var.tgw_id
}