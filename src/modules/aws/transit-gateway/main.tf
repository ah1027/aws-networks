#####################################################################
# Terraform script to create AWS Transit Gateway (TGW)
#####################################################################

# Transit Gateway creation (TGW)

resource "aws_ec2_transit_gateway" "Main-TGW" {
  description                     = "VPC Transit Gateway - PRD"
  default_route_table_association = "enable"
  default_route_table_propagation = "enable"

  tags = merge(
    var.all_tags,
    {
      Name      = var.tag_name_prefix
    }
  )
}

output "tgw_id" {
  value       = aws_ec2_transit_gateway.Main-TGW.id
  description = "Transit Gateway ID"
}

# default route tables:

resource "aws_ec2_transit_gateway_route_table" "spoke_default" {
  transit_gateway_id              = aws_ec2_transit_gateway.Main-TGW.id

  tags = merge(
    var.all_tags,
    {
      Name = "${var.tag_name_prefix}-SPOKES-RT"
    }
  )
}

resource "aws_ec2_transit_gateway_route_table" "vpc_sec" {
  transit_gateway_id              = aws_ec2_transit_gateway.Main-TGW.id

  tags = merge(
    var.all_tags,
    {
      Name = "${var.tag_name_prefix}-VPC-SEC-RT"
    }
  )
}

resource "aws_ec2_transit_gateway_route_table" "mgmt" {
  transit_gateway_id              = aws_ec2_transit_gateway.Main-TGW.id

  tags = merge(
    var.all_tags,
    {
      Name = "${var.tag_name_prefix}-VPC-MGMT-RT"
    }
  )
}

resource "aws_ec2_transit_gateway_route_table" "vpn_campus" {
  transit_gateway_id              = aws_ec2_transit_gateway.Main-TGW.id

  tags = merge(
    var.all_tags,
    {
      Name = "${var.tag_name_prefix}-VPN-CAMPUS-SRX"
    }
  )
}





output "spoke_route_table_ID" {
  value       = aws_ec2_transit_gateway_route_table.spoke_default.id
  description = "spoke route table ID"
}

output "vpc_sec_rt_id" {
  value       = aws_ec2_transit_gateway_route_table.vpc_sec.id
  description = "spoke route table ID"
}

output "vpc_mgmt_rt_id" {
  value       = aws_ec2_transit_gateway_route_table.mgmt.id
  description = "spoke route table ID"
}

output "vpn_campus_rt_id" {
  value       = aws_ec2_transit_gateway_route_table.vpn_campus.id
  description = "spoke route table ID"
}

# Unfortunately terraform doesn't have a way to manually change the default route tables for propagation & association.
# The workaround for this is run a AWS-CLI commands to manually set it. Has to be ran AFTER the route tables are created.
# aws ec2 modify-transit-gateway --transit-gateway-id <id> --options <option1=1,option2=2>

resource "null_resource" "main" {
  provisioner "local-exec" {
    command = "aws ec2 modify-transit-gateway --transit-gateway-id ${aws_ec2_transit_gateway.Main-TGW.id} --options AssociationDefaultRouteTableId=${aws_ec2_transit_gateway_route_table.spoke_default.id},PropagationDefaultRouteTableId=${aws_ec2_transit_gateway_route_table.vpc_sec.id}"
  }
}
