# main.tf

# attach vpc_sec to TGW
resource "aws_ec2_transit_gateway_vpc_attachment" "tgw-att-vpc-sec" {
  subnet_ids                                      = [ var.vpc_sec_att_sub1, var.vpc_sec_att_sub2 ]
  transit_gateway_id                              = var.tgw_id
  vpc_id                                          = var.vpc_sec_id
  # set vpc attachment to not joint hte default route table of the transit gateway. Default is True.
  transit_gateway_default_route_table_association = false
  transit_gateway_default_route_table_propagation = false

  tags = merge(
    var.all_tags,
    {
      Name     = "tgw_att_vpc_sec"
    }
  ) 
}

# associate with the TGW SEC route table

resource "aws_ec2_transit_gateway_route_table_association" "tgw-att-vpc-sec" {
  transit_gateway_attachment_id  = aws_ec2_transit_gateway_vpc_attachment.tgw-att-vpc-sec.id
  transit_gateway_route_table_id = var.tgw_vpc_sec_rt_id
}


# attach vpc_mgmt to TGW

resource "aws_ec2_transit_gateway_vpc_attachment" "tgw-att-vpc-mgmt" {
  subnet_ids                                      = [ var.vpc_mgmt_att_sub1, var.vpc_mgmt_att_sub2 ]
  transit_gateway_id                              = var.tgw_id
  vpc_id                                          = var.vpc_mgmt_id
  # set vpc attachment to not joint the default route table of the transit gateway. Default is True.
  transit_gateway_default_route_table_association = false
  transit_gateway_default_route_table_propagation = true

  tags = merge(
    var.all_tags,
    {
      Name     = "tgw_att_vpc_mgmt"
    }
  ) 
}

# associate with the TGW SEC route table

resource "aws_ec2_transit_gateway_route_table_association" "tgw-att-vpc-mgmt" {
  transit_gateway_attachment_id  = aws_ec2_transit_gateway_vpc_attachment.tgw-att-vpc-mgmt.id
  transit_gateway_route_table_id = var.tgw_vpc_mgmt_rt_id
}


output "vpc_mgmt_att" {
  value = aws_ec2_transit_gateway_route_table_association.tgw-att-vpc-mgmt.id
}

output "vpc_sec_att" {
  value = aws_ec2_transit_gateway_vpc_attachment.tgw-att-vpc-sec.id
}