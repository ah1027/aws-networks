# variables.tf 

# from module.transit-gateway
variable "tgw_id" { default = "" }
variable "tgw_vpc_sec_rt_id" { default = "" }
variable "tgw_vpc_mgmt_rt_id" { default = "" }

# from module.vpc
variable "vpc_sec_id" { default = "" }
variable "vpc_sec_att_sub1" { default = "" }
variable "vpc_sec_att_sub2" { default = "" }

variable "vpc_mgmt_id" { default = "" }
variable "vpc_mgmt_att_sub1" { default = "" }
variable "vpc_mgmt_att_sub2" { default = "" }

variable "all_tags" {
  default = {}
  description = "all tags"
  type = map(string)
}