# transit-gateway-routes variables.tf

variable "tgw_vpn_campus_rt" {}
variable "tgw_vpc_mgmt_rt" {}
variable "tgw_vpc_sec_rt" {} 
variable "tgw_vpc_spokes_rt" {} 

variable "tgw_att_vpc_sec" {}
variable "tgw_att_vpc_mgmt" {}
variable "tgw_att_vpn_campus" {}

variable "vpn_routes" {
  type = list(string)
}


variable "all_tags" {
  default = {}
  description = "all tags"
  type = map(string)
}