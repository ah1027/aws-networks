# transit-gateway-routes main.tf

# Routes are done in the following Format:

/*

resource "aws_ec2_transit_gateway_route" "name_here" {
  destination_cidr_block         = "0.0.0.0/0" 
  transit_gateway_attachment_id  = var.tgw_att_vpc_sec <- Next hop.
  transit_gateway_route_table_id = var.tgw-vpc_spokes-rt <- Which route table
}

*/

#####################
# SPOKE ROUTES
#####################
/*
List of routes for route table TGW-SPOKES-RT:

0.0.0.0/0   -> VPC-SEC

*/

# Spokes Default Route
resource "aws_ec2_transit_gateway_route" "vpc_spokes_default" {
  destination_cidr_block         = "0.0.0.0/0"
  transit_gateway_attachment_id  = var.tgw_att_vpc_sec
  transit_gateway_route_table_id = var.tgw_vpc_spokes_rt
}

#####################
# VPN ROUTES
#####################
/*
List of routes for route table TGW-VPN-CAMPUS-SRX:

0.0.0.0/0   -> VPC-SEC
10.143.16.0 -> VPC-MGMT

*/


# VPN Default route
resource "aws_ec2_transit_gateway_route" "vpn_campus_default" {
  destination_cidr_block         = "0.0.0.0/0"
  transit_gateway_attachment_id  = var.tgw_att_vpc_sec
  transit_gateway_route_table_id = var.tgw_vpn_campus_rt
}



#####################
# MGMT ROUTES
#####################
/*
List of routes for route table TGW-VPC-MGMT-RT:

0.0.0.0/0   -> VPC-SEC 

Rest are propagated.

*/

resource "aws_ec2_transit_gateway_route" "vpc_mgmt_default" {
  destination_cidr_block         = "0.0.0.0/0"
  transit_gateway_attachment_id  = var.tgw_att_vpc_sec
  transit_gateway_route_table_id = var.tgw_vpc_mgmt_rt
}

#####################
# SEC ROUTES
#####################
/*
List of routes for route table TGW-VPC-SEC-RT:

144.173.x.x -> VPN-CAMPUS

Rest are propagated.

*/
# VPN CAMPUS - DC-PRD
resource "aws_ec2_transit_gateway_route" "vpn_route1" {
  destination_cidr_block         = "144.173.6.0/24"
  transit_gateway_attachment_id  = var.tgw_att_vpn_campus
  transit_gateway_route_table_id = var.tgw_vpc_sec_rt
}
# VPN CAMPUS - VIA VPN Range 1
resource "aws_ec2_transit_gateway_route" "vpn_route2" {
  destination_cidr_block         = "10.46.128.0/23"
  transit_gateway_attachment_id  = var.tgw_att_vpn_campus
  transit_gateway_route_table_id = var.tgw_vpc_sec_rt
}
# VPN CAMPUS - VIA VPN Range 2
resource "aws_ec2_transit_gateway_route" "vpn_route3" {
  destination_cidr_block         = "10.46.130.0/24"
  transit_gateway_attachment_id  = var.tgw_att_vpn_campus
  transit_gateway_route_table_id = var.tgw_vpc_sec_rt
}

# VPN CAMPUS - See in variables, runs through the list and adds them all.
resource "aws_ec2_transit_gateway_route" "vpn_route4" {
  count                   = length(var.vpn_routes)

  destination_cidr_block  = var.vpn_routes[count.index]
  transit_gateway_attachment_id  = var.tgw_att_vpn_campus
  transit_gateway_route_table_id = var.tgw_vpc_sec_rt
}