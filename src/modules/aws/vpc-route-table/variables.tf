# variables.tf
variable "vpc_sec_id" { default = "" }
variable "mgmt_vpc_route_table_id" { default = "" }
variable "vpc_sec_mgmt_sub1" { default = "" }
variable "vpc_sec_mgmt_sub2" { default = "" }
variable "vpc_sec_data_sub1" { default = "" }
variable "vpc_sec_data_sub2" { default = "" }
variable "vpc_sec_relay_sub1" { default = "" }
variable "vpc_sec_relay_sub2" { default = "" }

variable "tag_name_prefix" {
  description = "Provide a common tag prefix value that will be used in the name tag for all resources"
  default     = "TGW"
}

variable "tag_name_unique" {
  description = "Provide a unique tag prefix value that will be used in the name tag for each modules resources"
  default     = "terraform"
}

variable "all_tags" {
  default = {}
  description = "all tags"
  type = map(string)
}