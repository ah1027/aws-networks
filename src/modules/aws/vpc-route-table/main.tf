# vpc-route-table main.tf

# create default route table for mgmt vpc

resource "aws_default_route_table" "vpc_mgmt" {
  default_route_table_id = var.mgmt_vpc_route_table_id

  tags = merge(
    var.all_tags,
    {
      Name = "mgmt_rt"
    }
  ) 
}


resource "aws_route_table" "vpc_sec_relay" {
  vpc_id = var.vpc_sec_id

  tags = merge(
    var.all_tags,
    {
      Name = "${var.tag_name_prefix}-${var.tag_name_unique}-relay-rt"
    }
  ) 
}


resource "aws_route_table" "vpc_sec_data_and_mgmt" {
  vpc_id = var.vpc_sec_id

  tags = merge(
    var.all_tags,
    {
      Name = "${var.tag_name_prefix}-${var.tag_name_unique}-data-and-mgmt-rt"
    }
  ) 
}




# associations for vpc_sec

resource "aws_route_table_association" "data_rt_association1" {
  subnet_id      = var.vpc_sec_data_sub1
  route_table_id = aws_route_table.vpc_sec_data_and_mgmt.id
}

resource "aws_route_table_association" "data_rt_association2" {
  subnet_id      = var.vpc_sec_data_sub2
  route_table_id = aws_route_table.vpc_sec_data_and_mgmt.id
}

resource "aws_route_table_association" "mgmt_rt_association1" {
  subnet_id      = var.vpc_sec_mgmt_sub1
  route_table_id = aws_route_table.vpc_sec_data_and_mgmt.id
}

resource "aws_route_table_association" "mgmt_rt_association2" {
  subnet_id      = var.vpc_sec_mgmt_sub2
  route_table_id = aws_route_table.vpc_sec_data_and_mgmt.id
}

resource "aws_route_table_association" "relay_rt_association1" {
  subnet_id      = var.vpc_sec_relay_sub1
  route_table_id = aws_route_table.vpc_sec_relay.id
}

resource "aws_route_table_association" "relay_rt_association2" {
  subnet_id      = var.vpc_sec_relay_sub2
  route_table_id = aws_route_table.vpc_sec_relay.id
}

# output

output "vpc_mgmt_rt" {
  value = aws_default_route_table.vpc_mgmt.id
}

output "vpc_sec_relay_rt" {
  value = aws_route_table.vpc_sec_relay.id
}

output "vpc_sec_main_rt" {
  value = aws_route_table.vpc_sec_data_and_mgmt.id
}