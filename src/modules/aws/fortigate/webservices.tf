# fortigate / webservices.tf

# 0 is used for the firewalls.
# local.private_ips_fgt1[1]

/* example config:

resource "aws_eip" "websub1" {
  network_interface         = aws_network_interface.eni-fgt1-data.id 
  associate_with_private_ip = local.private_ips_fgt1[1]
  tags = {
    Name  = "websub1"
  }
}


*/