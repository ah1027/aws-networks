# fortigate main.tf

##############################################################################################################
#
# AWS Transit Gateway
# FortiGate setup with Active/Passive in Multiple Availability Zones
# https://github.com/fortinet/fortigate-terraform-deploy aws/6.4/transitgwy
##############################################################################################################

##############################################################################################################
# GENERAL
##############################################################################################################

# Create keypair

resource "aws_key_pair" "main" {
  key_name   = "fgt-key"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAABJQAAAQB9Z7FjQEAL5R4ZwGjZcG7TRw5aruEeFUXHjqFTtO/xx6CTrREVSFSi+f1tooL8YJkS8qcLq/fgjq6eZnr9LUkCZxnzX7YBZg6fs6wYhYRkSgjHWO1roVT/sFJPgL84pInRE0Deeuzna/T+HzIOgzxiYJUDejLOPPLzPxULDdPIvgefKfXeUdYSw+0CbTeLv4eEN/1i/EH6777lve+uMD1EK50TrdtqyfCQLIanV/jMDqdMH9dhIYwvrjYApqSCNvQ9zE0pVQ8isSlypIk7jqte/CWm7At2nr8XQmtNKqt8xNwBfcQ+akqy9Ha1RcGox29K5UR8ftLYxqBgLgXfyGwB fgt-key"
  
  tags = {
    Name = "fgt-key"
  }
}


locals {

  ip_range = [
    for i in range(4, 11) : i
  ]
  private_ips_fgt1 = [
    for ip in local.ip_range : cidrhost(var.security_vpc_data_subnet_cidr1, ip)
  ]
  private_ips_fgt2 = [
    for ip in local.ip_range : cidrhost(var.security_vpc_data_subnet_cidr2, ip)
  ]


  data_fgt1 = [local.private_ips_fgt1[0]]
  data_fgt2 = [local.private_ips_fgt2[0]]
}






# Security Groups
## Need to create 4 of them as our Security Groups are linked to a VPC

resource "aws_security_group" "NSG-mgmt-ssh-icmp-https" {
  name        = "NSG-mgmt-ssh-icmp-https"
  description = "Allow SSH, HTTPS and ICMP traffic"
  vpc_id      = var.vpc_mgmt_id

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = -1 # the ICMP type number for 'Echo Reply'
    to_port     = -1 # the ICMP code
    protocol    = "icmp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name     = "NSG-mgmt-ssh-icmp-https"
  }
}

resource "aws_security_group" "NSG-vpc-sec-ssh-icmp-https" {
  name        = "NSG-vpc-sec-ssh-icmp-https"
  description = "Allow SSH, HTTPS and ICMP traffic"
  vpc_id      = var.vpc_sec_id

  ingress {
    description = "Allow remote access to FGT"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name     = "NSG-vpc-sec-ssh-icmp-https"
  }
}

##############################################################################################################
# FORTIGATES VM
##############################################################################################################
# Create the IAM role/profile for the API Call
resource "aws_iam_instance_profile" "APICall_profile" {
  name = "APICall_profile"
  role = aws_iam_role.APICallrole.name
}

resource "aws_iam_role" "APICallrole" {
  name = "APICall_role"

  assume_role_policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": "sts:AssumeRole",
            "Principal": {
              "Service": "ec2.amazonaws.com"
            },
            "Effect": "Allow",
            "Sid": ""
        }
    ]
}
EOF
}

resource "aws_iam_policy" "APICallpolicy" {
  name        = "APICall_policy"
  path        = "/"
  description = "Policies for the FGT APICall Role"

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement":
      [
        {
          "Effect": "Allow",
          "Action":
            [
              "ec2:Describe*",
              "ec2:AssociateAddress",
              "ec2:AssignPrivateIpAddresses",
              "ec2:UnassignPrivateIpAddresses",
              "ec2:ReplaceRoute"
            ],
            "Resource": "*"
        }
      ]
}
EOF
}

resource "aws_iam_policy_attachment" "APICall-attach" {
  name       = "APICall-attachment"
  roles      = [aws_iam_role.APICallrole.name]
  policy_arn = aws_iam_policy.APICallpolicy.arn
}


# Create all the eni interfaces
resource "aws_network_interface" "eni-fgt1-data" {
  subnet_id         = var.vpc_sec_data_sub1
  security_groups   = [aws_security_group.NSG-vpc-sec-ssh-icmp-https.id]
  private_ips       = local.private_ips_fgt1
  source_dest_check = false
  tags = {
    Name = "${var.tag_name_prefix}-fgt1-enidata"
  }
}

resource "aws_network_interface" "eni-fgt2-data" {
  subnet_id         = var.vpc_sec_data_sub2
  security_groups   = [aws_security_group.NSG-vpc-sec-ssh-icmp-https.id]
  source_dest_check = false
  tags = {
    Name = "${var.tag_name_prefix}-fgt2-enidata"
  }
}

resource "aws_network_interface" "eni-fgt1-hb" {
  subnet_id         = var.vpc_sec_heart_sub1 
  security_groups   = [aws_security_group.NSG-vpc-sec-ssh-icmp-https.id]
  private_ips       = [cidrhost(var.security_vpc_heartbeat_subnet_cidr1, 10)]
  source_dest_check = false
  tags = {
    Name = "${var.tag_name_prefix}-fgt1-enihb"
  }
}

resource "aws_network_interface" "eni-fgt2-hb" {
  subnet_id         = var.vpc_sec_heart_sub2
  security_groups   = [aws_security_group.NSG-vpc-sec-ssh-icmp-https.id]
  private_ips       = [cidrhost(var.security_vpc_heartbeat_subnet_cidr2, 10)]
  source_dest_check = false
  tags = {
    Name = "${var.tag_name_prefix}-fgt2-enihb"
  }
}

resource "aws_network_interface" "eni-fgt1-mgmt" {
  subnet_id         = var.vpc_sec_mgmt_sub1
  security_groups   = [aws_security_group.NSG-vpc-sec-ssh-icmp-https.id]
  source_dest_check = false
  tags = {
    Name = "${var.tag_name_prefix}-fgt1-enimgmt"
  }
}

resource "aws_network_interface" "eni-fgt2-mgmt" {
  subnet_id         = var.vpc_sec_mgmt_sub2
  security_groups   = [aws_security_group.NSG-vpc-sec-ssh-icmp-https.id]
  source_dest_check = false
  tags = {
    Name = "${var.tag_name_prefix}-fgt2-enimgmt"
  }
}

# Create and attach the eip to the units
resource "aws_eip" "eip-mgmt1" {
  depends_on        = [aws_instance.fgt1]
  vpc               = true
  network_interface = aws_network_interface.eni-fgt1-mgmt.id
  tags = {
    Name = "${var.tag_name_prefix}-fgt1-eip-mgmt"
  }
}

resource "aws_eip" "eip-mgmt2" {
  depends_on        = [aws_instance.fgt2]
  vpc               = true
  network_interface = aws_network_interface.eni-fgt2-mgmt.id
  tags = {
    Name = "${var.tag_name_prefix}-fgt2-eip-mgmt"
  }
}

resource "aws_eip" "eip-shared" {
  depends_on        = [aws_instance.fgt1]
  vpc               = true
  network_interface = aws_network_interface.eni-fgt1-data.id
  tags = {
    Name = "${var.tag_name_prefix}-eip-cluster"
  }
}

# Create the instances
resource "aws_instance" "fgt1" {
  ami                  = var.license_type == "byol" ? var.fgtvmbyolami[var.region] : var.fgt-ond-amis[var.region]
  instance_type        = var.instance_type
  availability_zone    = var.availability_zone1
  key_name             = aws_key_pair.main.id
  user_data            = data.template_file.fgt_userdata1.rendered
  iam_instance_profile = aws_iam_instance_profile.APICall_profile.name
  network_interface {
    device_index         = 0
    network_interface_id = aws_network_interface.eni-fgt1-data.id
  }
  network_interface {
    device_index         = 1
    network_interface_id = aws_network_interface.eni-fgt1-hb.id
  }
  network_interface {
    device_index         = 2
    network_interface_id = aws_network_interface.eni-fgt1-mgmt.id
  }
  tags = merge(
    var.all_tags,
    {
      Name = "${var.tag_name_prefix}-${var.tag_name_unique}-fgt1"
    }
  )
}

resource "aws_instance" "fgt2" {
  ami                  = var.license_type == "byol" ? var.fgtvmbyolami[var.region] : var.fgt-ond-amis[var.region]
  instance_type        = var.instance_type
  availability_zone    = var.availability_zone2
  key_name             = aws_key_pair.main.id
  user_data            = data.template_file.fgt_userdata2.rendered
  iam_instance_profile = aws_iam_instance_profile.APICall_profile.name
  network_interface {
    device_index         = 0
    network_interface_id = aws_network_interface.eni-fgt2-data.id
  }
  network_interface {
    device_index         = 1
    network_interface_id = aws_network_interface.eni-fgt2-hb.id
  }
  network_interface {
    device_index         = 2
    network_interface_id = aws_network_interface.eni-fgt2-mgmt.id
  }
  tags = merge(
    var.all_tags,
    {
      Name = "${var.tag_name_prefix}-${var.tag_name_unique}-fgt2"
    }
  )
}

data "template_file" "fgt_userdata1" {
  template = file(abspath("${path.module}/fgt-userdata.tpl"))

  vars = {
    fgt_id               = "FGT-Active"
    type                 = "${var.license_type}"
    license_file         = abspath("${path.module}/${var.license}")
    fgt_data_ip          = join("/", [element(tolist(local.data_fgt1), 0), cidrnetmask("${var.security_vpc_data_subnet_cidr1}")])
    fgt_heartbeat_ip     = join("/", [element(tolist(aws_network_interface.eni-fgt1-hb.private_ips), 0), cidrnetmask("${var.security_vpc_heartbeat_subnet_cidr1}")])
    fgt_mgmt_ip          = join("/", [element(tolist(aws_network_interface.eni-fgt1-mgmt.private_ips), 0), cidrnetmask("${var.security_vpc_mgmt_subnet_cidr1}")])
    data_gw              = cidrhost(var.security_vpc_data_subnet_cidr1, 1)
    mgmt_cidr            = var.mgmt_cidr
    password             = var.password
    mgmt_gw              = cidrhost(var.security_vpc_mgmt_subnet_cidr1, 1)
    fgt_priority         = "255"
    fgt-remote-heartbeat = element(tolist(aws_network_interface.eni-fgt2-hb.private_ips), 0)
  }
}

data "template_file" "fgt_userdata2" {
  template = file(abspath("${path.module}/fgt-userdata.tpl"))

  vars = {
    fgt_id               = "FGT-Passive"
    type                 = "${var.license_type}"
    license_file         = abspath("${path.module}/${var.license2}")
    fgt_data_ip          = join("/", [element(tolist(aws_network_interface.eni-fgt2-data.private_ips), 0), cidrnetmask("${var.security_vpc_data_subnet_cidr2}")])
    fgt_heartbeat_ip     = join("/", [element(tolist(aws_network_interface.eni-fgt2-hb.private_ips), 0), cidrnetmask("${var.security_vpc_heartbeat_subnet_cidr2}")])
    fgt_mgmt_ip          = join("/", [element(tolist(aws_network_interface.eni-fgt2-mgmt.private_ips), 0), cidrnetmask("${var.security_vpc_mgmt_subnet_cidr2}")])
    data_gw              = cidrhost(var.security_vpc_data_subnet_cidr2, 1)
    mgmt_cidr            = var.mgmt_cidr
    password             = var.password
    mgmt_gw              = cidrhost(var.security_vpc_mgmt_subnet_cidr2, 1)
    fgt_priority         = "100"
    fgt-remote-heartbeat = element(tolist(aws_network_interface.eni-fgt1-hb.private_ips), 0)
  }
}



