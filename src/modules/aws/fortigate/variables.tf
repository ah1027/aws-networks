# fortigate variables.tf

variable "tag_name_prefix" {
  description = "Provide a common tag prefix value that will be used in the name tag for all resources"
  default     = "TGW"
}

variable "tag_name_unique" {
  description = "Provide a unique tag prefix value that will be used in the name tag for each modules resources"
  default     = "terraform"
}

variable "availability_zone1" {
  description = "Provide the first availability zone to create the subnets in"
  default     = "eu-west-2a"
}

variable "availability_zone2" {
  description = "Provide the second availability zone to create the subnets in"
  default     = "eu-west-2b"
}

variable "vpc_sec_id" { default = "" }
variable "vpc_mgmt_id" { default = "" }
variable "vpc_sec_data_sub1" { default = "" }
variable "vpc_sec_data_sub2" { default = "" }
variable "vpc_sec_heart_sub1" { default = "" }
variable "vpc_sec_heart_sub2" { default = "" }
variable "vpc_sec_mgmt_sub1" { default = "" }
variable "vpc_sec_mgmt_sub2" { default = "" }
variable "security_vpc_cidr" { default = "" }
variable "security_vpc_data_subnet_cidr1" { default = "" }
variable "security_vpc_data_subnet_cidr2" { default = "" }
variable "security_vpc_relay_subnet_cidr1" { default = "" }
variable "security_vpc_relay_subnet_cidr2" { default = "" }
variable "security_vpc_mgmt_subnet_cidr1" { default = "" }
variable "security_vpc_mgmt_subnet_cidr2" { default = "" }
variable "security_vpc_heartbeat_subnet_cidr1" { default = "" }
variable "security_vpc_heartbeat_subnet_cidr2" { default = "" }
variable "mgmt_cidr" { default = "" }
variable "mgmt_private_subnet_cidr1" { default = "" }
variable "mgmt_private_subnet_cidr2" { default = "" }

variable "all_tags" {
  default = {}
  description = "all tags"
  type = map(string)
}
