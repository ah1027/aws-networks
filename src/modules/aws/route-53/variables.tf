# route-53 variables.tf

variable "all_tags" {
  default = {}
  description = "all tags"
  type = map(string)
}