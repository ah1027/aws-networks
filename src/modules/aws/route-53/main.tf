# route-53 main.tf

locals {
  firewall_dns = {
    fgt1-mgmt = "${data.aws_eip.fgt1-mgmt.public_ip}",
    fgt2-mgmt = "${data.aws_eip.fgt2-mgmt.public_ip}"
  }
}

data "aws_eip" "fgt1-mgmt" {
  filter {
    name    = "tag:Name"
    values  = ["TGW-fgt1-eip-mgmt"]
  }  
}

data "aws_eip" "fgt2-mgmt" {
  filter {
    name    = "tag:Name"
    values  = ["TGW-fgt2-eip-mgmt"]
  }  
}



resource "aws_route53_zone" "main" {
  name = "ip.cloud.exeter.ac.uk"

  tags = merge(
    var.all_tags,
    {
      Name = "ip.cloud.exeter.ac.uk"
    }
  )
}


resource "aws_route53_record" "firewall_dns" {
  for_each = local.firewall_dns
  zone_id = aws_route53_zone.main.zone_id
  name    = each.key
  type    = "A"
  ttl     = "300"
  records = [each.value]
}