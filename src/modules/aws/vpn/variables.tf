# vpn variables.tf

variable "tgw_id" {}
variable "tgw_vpn_rt" {}
variable "tunnel1_key" {}
variable "tunnel2_key" {}
variable "tunnel1_cidr" {}
variable "tunnel2_cidr" {}

variable "all_tags" {
  default = {}
  description = "all tags"
  type = map(string)
}