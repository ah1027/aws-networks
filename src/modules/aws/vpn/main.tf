# vpn main.tf

locals {
  new_tags = merge(
    var.all_tags,
    {
      Name = "tgw_att_vpn_campus"
    }
  )
}

# Create Customer Gateway (non aws)
resource "aws_customer_gateway" "campus_srx" {
  ip_address = "144.173.160.243"
  type       = "ipsec.1"
  bgp_asn    = 65000

  tags = merge(
    var.all_tags,
    {
      Name = "UoE-Campus-SRX"
    }
  )
}

# Create VPN 
resource "aws_vpn_connection" "campus_srx" {
  customer_gateway_id                    = aws_customer_gateway.campus_srx.id
  transit_gateway_id                     = var.tgw_id
  static_routes_only                     = true
  
  # Tunnel 1
  tunnel1_inside_cidr                    = var.tunnel1_cidr
  tunnel1_preshared_key                  = var.tunnel1_key
  tunnel1_ike_versions                   = [ "ikev1" ] 
  # Tunnel 1 - Phase 1
  tunnel1_phase1_dh_group_numbers        = [ 2 ]
  tunnel1_phase1_integrity_algorithms    = [ "SHA1" ]
  tunnel1_phase1_encryption_algorithms   = [ "AES128" ]
  # Tunnel 1 - Phase 2
  tunnel1_phase2_dh_group_numbers        = [ 2 ]
  tunnel1_phase2_integrity_algorithms    = [ "SHA1" ]
  tunnel1_phase2_encryption_algorithms   = [ "AES128" ]
  # Tunnel 2
  tunnel2_inside_cidr                    = var.tunnel2_cidr
  tunnel2_preshared_key                  = var.tunnel2_key
  tunnel2_ike_versions                   = [ "ikev1" ]
  # Tunnel 2 - Phase 1
  tunnel2_phase1_dh_group_numbers        = [ 2 ]
  tunnel2_phase1_integrity_algorithms    = [ "SHA1" ]
  tunnel2_phase1_encryption_algorithms   = [ "AES128" ] 
  # Tunnel 2 - Phase 2
  tunnel2_phase2_dh_group_numbers        = [ 2 ]
  tunnel2_phase2_integrity_algorithms    = [ "SHA1" ]
  tunnel2_phase2_encryption_algorithms   = [ "AES128" ]

  type                                   = "ipsec.1"

  tags = merge(
    var.all_tags,
    {
      Name = "VPN-Campus"
    }
  ) 
}

# create tag for attachment

resource "aws_ec2_tag" "tgw-vpn-att" {
  for_each    = local.new_tags
  resource_id = aws_vpn_connection.campus_srx.transit_gateway_attachment_id
  key         = each.key
  value       = each.value
}

# Annoyingly the VPN associates with the default route table with no option to override it when creating the VPN... So we have to use null resource again to remove it before re-adding it.
# Look up the default association:
data "aws_ec2_transit_gateway_route_table" "default_table" {
  filter {
    name   = "tag:Name"
    values = [ "TGW-SPOKES-RT" ] 
  }
}

# Delete association
resource "null_resource" "del-association" {
  provisioner "local-exec" {
    command = "aws ec2 disassociate-transit-gateway-route-table --transit-gateway-route-table-id ${data.aws_ec2_transit_gateway_route_table.default_table.id} --transit-gateway-attachment-id ${aws_vpn_connection.campus_srx.transit_gateway_attachment_id} "
  }
}

# sleep after delete
resource "time_sleep" "sleep" {
  create_duration = "60s"
  depends_on = [ null_resource.del-association ]
}


# Then attach to the right one with a delay as it takes some time...

resource "aws_ec2_transit_gateway_route_table_association" "tgw-vpn-att" {
  transit_gateway_attachment_id  = aws_vpn_connection.campus_srx.transit_gateway_attachment_id
  transit_gateway_route_table_id = var.tgw_vpn_rt
  depends_on = [ time_sleep.sleep ]
}

output "vpn_att_campus" {
  value = aws_vpn_connection.campus_srx.transit_gateway_attachment_id
}