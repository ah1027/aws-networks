#####################################################################
# VPC Terraform script to create shared network VPCs 
#####################################################################

##############################
# VPC SECURITY
##############################

resource "aws_vpc" "vpc_sec" {
  cidr_block            = var.security_vpc_cidr
  enable_dns_support    = true
  enable_dns_hostnames  = true

  tags = {
      Name = "${var.tag_name_prefix}-vpc_sec"
    }

}

# IGW for VPC SECURITY

resource "aws_internet_gateway" "igw_sec" {
  vpc_id = aws_vpc.vpc_sec.id

  tags = {
      Name = "${var.tag_name_prefix}-${var.tag_name_unique}-igw_sec"
    }
}

# Subnets for VPC SECURITY

resource "aws_subnet" "data_subnet1" {
  vpc_id            = aws_vpc.vpc_sec.id
  cidr_block        = var.security_vpc_data_subnet_cidr1
  availability_zone = var.availability_zone1

  tags = {
      Name = "${var.tag_name_prefix}-${var.tag_name_unique}-data-subnet1"
    }

}

resource "aws_subnet" "data_subnet2" {
  vpc_id            = aws_vpc.vpc_sec.id
  cidr_block        = var.security_vpc_data_subnet_cidr2
  availability_zone = var.availability_zone2

  tags = {
      Name = "${var.tag_name_prefix}-${var.tag_name_unique}-data-subnet2"
    }
}

resource "aws_subnet" "relay_subnet1" {
  vpc_id            = aws_vpc.vpc_sec.id
  cidr_block        = var.security_vpc_relay_subnet_cidr1
  availability_zone = var.availability_zone1

  tags = {
      Name = "${var.tag_name_prefix}-${var.tag_name_unique}-relay-subnet1"
    }
}

resource "aws_subnet" "relay_subnet2" {
  vpc_id            = aws_vpc.vpc_sec.id
  cidr_block        = var.security_vpc_relay_subnet_cidr2
  availability_zone = var.availability_zone2

  tags = {
      Name = "${var.tag_name_prefix}-${var.tag_name_unique}-relay-subnet2"
    }
}

resource "aws_subnet" "heartbeat_subnet1" {
  vpc_id            = aws_vpc.vpc_sec.id
  cidr_block        = var.security_vpc_heartbeat_subnet_cidr1
  availability_zone = var.availability_zone1

  tags = {
      Name = "${var.tag_name_prefix}-${var.tag_name_unique}-heartbeat-subnet1"
    }
}

resource "aws_subnet" "heartbeat_subnet2" {
  vpc_id            = aws_vpc.vpc_sec.id
  cidr_block        = var.security_vpc_heartbeat_subnet_cidr2
  availability_zone = var.availability_zone2

  tags = {
      Name = "${var.tag_name_prefix}-${var.tag_name_unique}-heartbeat-subnet2"
    }
}

resource "aws_subnet" "mgmt_subnet1" {
  vpc_id            = aws_vpc.vpc_sec.id
  cidr_block        = var.security_vpc_mgmt_subnet_cidr1
  availability_zone = var.availability_zone1

  tags = {
      Name = "${var.tag_name_prefix}-${var.tag_name_unique}-mgmt-subnet1"
    }
}

resource "aws_subnet" "mgmt_subnet2" {
  vpc_id            = aws_vpc.vpc_sec.id
  cidr_block        = var.security_vpc_mgmt_subnet_cidr2
  availability_zone = var.availability_zone2

  tags = {
      Name = "${var.tag_name_prefix}-${var.tag_name_unique}-mgmt-subnet2"
    }
}

#############################
# VPC MGMT
#############################

resource "aws_vpc" "spoke_mgmt" {
  cidr_block           = var.mgmt_cidr
  enable_dns_support   = true
  enable_dns_hostnames = true

  tags = {
      Name     = "${var.tag_name_prefix}-vpc-mgmt"
    }
}

# Subnets for VPC MGMT

resource "aws_subnet" "spoke_mgmt-priv1" {
  vpc_id            = aws_vpc.spoke_mgmt.id
  cidr_block        = var.mgmt_private_subnet_cidr1
  availability_zone = var.availability_zone1

  tags = {
      Name = "${aws_vpc.spoke_mgmt.tags.Name}-priv1"
    }
}

resource "aws_subnet" "spoke_mgmt-priv2" {
  vpc_id            = aws_vpc.spoke_mgmt.id
  cidr_block        = var.mgmt_private_subnet_cidr2
  availability_zone = var.availability_zone2

  tags = {
      Name = "${aws_vpc.spoke_mgmt.tags.Name}-priv2"
    }
}



###### Outputs

output "vpc_sec_igw" {
  value = aws_internet_gateway.igw_sec.id
  description = "VPC SEC IGW ID"
}

output "vpc_sec_att_sub1" {
  value = aws_subnet.relay_subnet1.id
  description = "VPC SEC Subnet 1 ID"
}

output "vpc_sec_att_sub2" {
  value = aws_subnet.relay_subnet2.id
  description = "VPC SEC Subnet 2 ID"
}
 
output "vpc_sec_id" {
  value = aws_vpc.vpc_sec.id 
  description = "VPC SEC ID"
}

output "vpc_mgmt_att_sub1" {
  value = aws_subnet.spoke_mgmt-priv1.id
  description = "VPC MGMT Subnet 1 ID"
}

output "vpc_mgmt_att_sub2" {
  value = aws_subnet.spoke_mgmt-priv2.id
  description = "VPC MGMT Subnet 2 ID"
}
 
output "vpc_mgmt_id" {
  value = aws_vpc.spoke_mgmt.id 
  description = "VPC MGMT ID"
}

output "vpc_mgmt_default_rt" {
  value = aws_vpc.spoke_mgmt.default_route_table_id
}


# outputs for vpc_sec subnet IDs: data, mgmt & relay
output "vpc_sec_data_sub1" {
  value = aws_subnet.data_subnet1.id
  description = "VPC SEC Subnet 1 ID"
}

output "vpc_sec_data_sub2" {
  value = aws_subnet.data_subnet2.id
  description = "VPC SEC Subnet 2 ID"
}

output "vpc_sec_mgmt_sub1" {
  value = aws_subnet.mgmt_subnet1.id
  description = "VPC SEC Subnet 1 ID"
}

output "vpc_sec_mgmt_sub2" {
  value = aws_subnet.mgmt_subnet2.id
  description = "VPC SEC Subnet 2 ID"
}

output "vpc_sec_relay_sub1" {
  value = aws_subnet.relay_subnet1.id
  description = "VPC SEC Subnet 1 ID"
}

output "vpc_sec_relay_sub2" {
  value = aws_subnet.relay_subnet2.id
  description = "VPC SEC Subnet 2 ID"
}

output "vpc_mgmt_cidr" {
  value = var.mgmt_cidr
}

output "vpc_sec_heart_sub1" {
  value = aws_subnet.heartbeat_subnet1.id
}

output "vpc_sec_heart_sub2" {
  value = aws_subnet.heartbeat_subnet2.id
}