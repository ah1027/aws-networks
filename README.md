# Networks prd - AWS terraform

## Configuration

### Credentials

[Terraform](https://learn.hashicorp.com/tutorials/terraform/install-cli) can use any form of credentials profile made available to it by the default [AWS CLI](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-install.html) configuration. The steps below describe how to use a AWS SSO specific credential profile, but any other valid form of profile can be used.

The following steps also describe browser based authentication via Azure AD. This `largely manual` approach can be used to base AWS SSO specific authentication on.

- Browser authenticate with Azure AD (login.microsoft)
- Update `~/.aws/config` with the relevant AWS SSO credentials

  ```ini
  [profile sso-profile-xyz]
  sso_start_url = <sso-url>
  sso_region = <sso-region>
  sso_account_id = <account-id>
  sso_role_name = <role>
  region = <eu-west-1>
  output = text
  ```

- Export the following environment variable in your local terminal to confirm which AWS profile AWS CLI and Terraform will use.

  ```bash
  # bash
  export AWS_PROFILE=sso-profile-xyz
  ```

  ```powershell
  # powershell
  $Env:AWS_PROFILE=sso-profile-xyz
  ```

- 3: Initiate login using AWS CLI that will then print out the details used to complete the following steps, i.e., authorisation URL and code

  ```bash
  aws sso login
  ```

- 4: A browser session to authorisation URL: https://device.sso.REGION.amazonaws.com/ will open

- 5: The authorisation code (FOO1-BAR2) is entered into the  Verification code field (and press submit marked `Next`)

- 6: Hold for browser redirection, but once complete press submit marked `Sign in to AWS CLI`

- 7: Execute the following command in the local terminal session to verify session access is correct.

  ```bash
  aws sts get-caller-identity
  ```

Please note that you must be using AWS CLI version above `2.1.23` in order to use SSO successfully.

## Execution

The following settings are required to execute the test script.

1. Add your `organization` name and associated workspace `prefix` to the workspace `state.tf` file.

2. Add your `organization` `token` to the workspace `.terraformrc` file.

3. Run these commands:

  ```bash
  export AWS_REGION="eu-west-2"
  export AWS_PROFILE="sso-profile-xyz"

  $ aws sso login
  $ sbin/init.sh
  ```

Additional info can be found on the wiki: [wiki](https://universityofexeteruk.sharepoint.com/sites/networks/Wiki/AWS.aspx)